CREATE TABLE public.gastos (
                id serial,
                descricao VARCHAR NOT NULL,
                CONSTRAINT gastos_pk PRIMARY KEY (id)
);


CREATE TABLE public.atividades (
                id serial,
                descricao VARCHAR NOT NULL,
                CONSTRAINT atividades_pk PRIMARY KEY (id)
);


CREATE TABLE public.especificidade (
                id serial,
                descricao VARCHAR NOT NULL,
                id_atividade INTEGER NOT NULL,
                CONSTRAINT especificidade_pk PRIMARY KEY (id)
);


CREATE TABLE public.fazenda (
                id serial,
                nome VARCHAR NOT NULL,
                m_quadrados INTEGER NOT NULL,
                CONSTRAINT fazenda_pk PRIMARY KEY (id)
);


CREATE TABLE public.controle (
                id serial,
                data_inicio DATE NOT NULL,
                resultado NUMERIC(2) NOT NULL,
                data_fim DATE NOT NULL,
                total_gasto NUMERIC(2) NOT NULL,
                valor_comercializacao NUMERIC(2) NOT NULL,
                id_fazenda INTEGER NOT NULL,
                id_especificidade INTEGER NOT NULL,
                CONSTRAINT controle_pk PRIMARY KEY (id)
);


CREATE TABLE public.controle_gasto (
                id serial,
                valor DATE NOT NULL,
                data DATE NOT NULL,
                id_gastos INTEGER NOT NULL,
                id_controle INTEGER NOT NULL,
                CONSTRAINT controle_gasto_pk PRIMARY KEY (id)
);


CREATE TABLE public.fazenda_atividade (
                id_fazenda INTEGER NOT NULL,
                id_atividade INTEGER NOT NULL,
                CONSTRAINT fazenda_atividade_pk PRIMARY KEY (id_fazenda, id_atividade)
);


ALTER TABLE public.controle_gasto ADD CONSTRAINT gastos_controle_gasto_fk
FOREIGN KEY (id_gastos)
REFERENCES public.gastos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fazenda_atividade ADD CONSTRAINT atividades_fazenda_atividade_fk
FOREIGN KEY (id_atividade)
REFERENCES public.atividades (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.especificidade ADD CONSTRAINT atividades_especificidade_fk
FOREIGN KEY (id_atividade)
REFERENCES public.atividades (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.controle ADD CONSTRAINT especificidade_controle_fk
FOREIGN KEY (id_especificidade)
REFERENCES public.especificidade (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.fazenda_atividade ADD CONSTRAINT fazenda_fazenda_atividade_fk
FOREIGN KEY (id_fazenda)
REFERENCES public.fazenda (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.controle ADD CONSTRAINT fazenda_controle_fk
FOREIGN KEY (id_fazenda)
REFERENCES public.fazenda (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.controle_gasto ADD CONSTRAINT controle_controle_gasto_fk
FOREIGN KEY (id_controle)
REFERENCES public.controle (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

