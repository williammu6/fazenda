<?php
    require_once('conf/db.php');
    $db = new Banco();
    $db->connect();

    switch($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            if ($_GET['ex']) {
                $sql = "DELETE FROM fazenda WHERE id=$_GET[ex]";
            }
            $db->db->query($sql);
            break;
    }

    $sql = "SELECT * FROM fazenda";
    $result = $db->db->query($sql);
?>

<!DOCTYPE html>
<head>
    <title>Fazenda</title>
    <meta name="author" content="farm"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Fazenda"/>
<!--    <link href="img/favicon.ico" rel="icon" type="image/x-icon"/>-->
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="awesome/css/font-awesome.css">
    <link rel="stylesheet" href="awesome/css/font-awesome.min.css">
</head>
<body>
    <h2>Ações</h2>
    <div class="acoes">
        <a href="cadastro/fazenda.php">Cadastrar Fazenda</a>
        <a href="cadastro/atividade.php">Cadastrar Atividade</a>
    </div>

    <h2>Fazendas</h2>
    <?php
        if($result->rowCount() == 0) {
            echo '<h3>Nenhuma fazenda cadastrada</h3>';
        } else {
    ?>
    <table>
        <tr>
            <th width="150">Nome</th>
            <th>Tamanho (m²)</th>
            <th colspan="2" width="50";">Ações</th>
        </tr>

        <?php
            foreach ($result as $fazenda) {
                $excluir = '<a href="index?ex=' . $fazenda['id'] . '"><i class="fa fa-trash-o"></i></a>';
                $editar = '<a href="edit?id=' . $fazenda['id'] . '"><i class="fa fa-pencil"></i></a>';

                echo "<tr>
                        <td style='text-align: left'>$fazenda[nome]</td>
                        <td>$fazenda[m_quadrados]</td>
                        <td>$excluir</td>
                        <td>$editar</td>
                    </tr>";
            }
        }
    ?>

    </table>
</body>

</html>
